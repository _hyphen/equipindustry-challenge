import React, { useState } from "react";
import Axios from "axios";
import { useHistory } from "react-router-dom";
import { FORM_ERROR } from "final-form";
import { connect } from "react-redux";
import ProductForm from "./ProductForm";

function NewProductForm({ currentCompany }) {
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const registerProduct = React.useCallback(
    (product) => {
      if (Object.keys(product).length !== 4) {
        return { [FORM_ERROR]: "Fields are required" };
      }
      setLoading(true);
      let success = false;
      function createProductSuccess(response) {
        console.log(response);

        success = true;
      }
      function createProductFail(error) {
        if (error.response) {
          return { [FORM_ERROR]: error.response.data.error };
        }
        return { [FORM_ERROR]: "Unknown error" };
      }
      function redirectIfSuccessful() {
        if (success) {
          history.push("/");
          return;
        }
        setLoading(false);
      }

      return Axios.post("/products", { ...product, companyId: currentCompany })
        .then(createProductSuccess, createProductFail)
        .finally(redirectIfSuccessful);
    },
    [history, currentCompany]
  );
  return (
    <div className="max-w-sm mx-auto">
      <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
        <h1 className="text-lg text-center mb-4 font-semibold">
          Create Product
        </h1>
        <ProductForm
          handleFormSubmit={registerProduct}
          submitButtonLabel="Create Product"
          loading={loading}
        />
      </div>
      <p className="text-center text-gray-500 text-xs">
        &copy;2020 hyphenized. All rights reserved.
      </p>
    </div>
  );
}

function mapStateToProps({ account }) {
  return {
    currentCompany: account.user.companyId,
  };
}

export default connect(mapStateToProps)(NewProductForm);
