import React, { useCallback, useState } from "react";
import { Form, Field } from "react-final-form";
import { connect } from "react-redux";
import { Redirect, useHistory } from "react-router";
import Axios from "axios";
import { FORM_ERROR } from "final-form";

function Login({ isLoggedIn, login }) {
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const handleLogin = useCallback(
    (user_data) => {
      let didLogin = false;
      setLoading(true);

      function loginSuccess(response) {
        const token = response.data.sessionId;
        try {
          localStorage.setItem("token", token);
        } catch (error) {
          console.log("Could not store token", error);
        }
        // in-memory login
        login(token);
        didLogin = true;
      }
      function loginFail(err) {
        if (err.response) {
          return { [FORM_ERROR]: err.response.data.error };
        }
      }
      function redirectIfLoggedIn() {
        if (didLogin) {
          history.push("/");
          return;
        }
        setLoading(false);
      }

      return Axios.post("/auth/login", user_data)
        .then(loginSuccess, loginFail)
        .finally(redirectIfLoggedIn);
    },
    [login, history]
  );

  if (isLoggedIn) return <Redirect to="/" />;
  return (
    <div className="max-w-xs mx-auto shadow-md rounded bg-white">
      <Form onSubmit={handleLogin}>
        {({ handleSubmit, submitError }) => (
          <form onSubmit={handleSubmit} className="w-full p-6 pb-8">
            <div className="mb-6">
              <div className="md:w-1/3">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="email"
                >
                  Email
                </label>
              </div>
              <div>
                <Field
                  component="input"
                  name="email"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="email"
                  type="email"
                  autoComplete="email"
                  placeholder="blabla@gmail.com"
                  autoFocus
                />
              </div>
            </div>
            <div className="mb-6">
              <div className="md:w-1/3">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="password"
                >
                  Password
                </label>
              </div>
              <div>
                <Field
                  component="input"
                  name="password"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="password"
                  type="password"
                  placeholder="******************"
                />
              </div>
            </div>
            <div className="flex md:items-center">
              <div className="w-1/3">
                <button
                  disabled={loading}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded  focus:shadow-outline"
                  type="submit"
                >
                  Sign In
                </button>
              </div>
              {submitError && (
                <p className="flex-1 text-red-500 text-xs">
                  Error: {submitError}
                </p>
              )}
            </div>
          </form>
        )}
      </Form>
    </div>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    login: (token) => dispatch({ type: "LOGIN", token }),
  };
}
function mapStateToProps({ account }) {
  return {
    isLoggedIn: account.isLoggedIn,
    token: account.token,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
