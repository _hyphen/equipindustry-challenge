import React from "react";
import { Route, Switch } from "react-router-dom";
import NewCompanyForm from "./NewCompanyForm";
import CompaniesIndex from "./CompaniesIndex";

function Companies() {
  return (
    <Switch>
      <Route exact path="/companies/new" component={NewCompanyForm} />
      <Route path="/companies" component={CompaniesIndex} />
    </Switch>
  );
}

export default Companies;
