import React from "react";
import { Link } from "react-router-dom";

function Home() {
  return (
    <div className="flex container mx-auto">
      <div className="flex flex-1 flex-wrap">
        <div className="bg-white flex-1 mx-4">
          <Link to="/products" className="p-4 block text-blue-600 uppercase">
            Products
          </Link>
        </div>
        <div className="bg-white flex-1 mx-4">
          <Link to="/companies" className="p-4 block text-blue-600 uppercase">
            Companies
          </Link>
        </div>
        <div className="text-blue-400 mt-4 p-4 bg-white w-full mx-4">
          <h1 className="text-2xl text-blue-500">Front End Challenge</h1>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus sed
            accusantium nesciunt alias.
          </p>
        </div>
      </div>
      <div className="flex w-1/3 items-center justify-center">
        <Link
          to="/projects"
          className="block text-blue-500 font-medium uppercase"
        >
          Start your project
        </Link>
      </div>
    </div>
  );
}

export default Home;
