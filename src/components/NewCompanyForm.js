import React, { useState } from "react";
import Axios from "axios";
import { useHistory } from "react-router-dom";
import { Form, Field } from "react-final-form";
import { FORM_ERROR } from "final-form";
import { connect } from "react-redux";

export function NewCompanyForm({ updateUserProfile }) {
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const registerCompany = React.useCallback(
    (company) => {
      if (Object.keys(company).length !== 2) {
        return { [FORM_ERROR]: "Fields are required" };
      }
      setLoading(true);
      let success = false;
      function createCompanySuccess(response) {
        updateUserProfile({ companyId: response.data.id });
        success = true;
      }
      function createCompanyFail(error) {
        if (error.response) {
          return { [FORM_ERROR]: error.response.data.error };
        }
        return { [FORM_ERROR]: "Unknown error" };
      }
      function redirectIfSuccessful() {
        if (success) {
          history.push("/");
          return;
        }
        setLoading(false);
      }

      return Axios.post("/companies/create_join", company)
        .then(createCompanySuccess, createCompanyFail)
        .finally(redirectIfSuccessful);
    },
    [history, updateUserProfile]
  );
  return (
    <div className="max-w-sm mx-auto">
      <Form onSubmit={registerCompany}>
        {({ handleSubmit, submitError }) => (
          <form
            onSubmit={handleSubmit}
            className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
          >
            <div className="mb-4">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="name"
              >
                Name
              </label>
              <Field
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                name="name"
                id="name"
                component="input"
                type="text"
                placeholder="Name"
                autoFocus
              />
            </div>
            <div className="mb-4">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="ruc"
              >
                R.U.C.
              </label>
              <Field
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                name="ruc"
                id="ruc"
                component="input"
                type="text"
                placeholder="R.U.C."
                autoFocus
              />
            </div>

            <div className="flex justify-between">
              <button
                disabled={loading}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="submit"
              >
                Create Company
              </button>
              {submitError && (
                <p className="w-2/4 text-red-500 text-xs">
                  Error: {submitError}
                </p>
              )}
            </div>
          </form>
        )}
      </Form>
      <p className="text-center text-gray-500 text-xs">
        &copy;2020 hyphenized. All rights reserved.
      </p>
    </div>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    updateUserProfile: (user) => dispatch({ type: "UPDATE_PROFILE", user }),
  };
}

export default connect(null, mapDispatchToProps)(NewCompanyForm);
