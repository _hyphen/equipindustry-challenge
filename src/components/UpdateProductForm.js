import React, { useState, useEffect } from "react";
import Axios from "axios";
import { useHistory } from "react-router-dom";
import { FORM_ERROR } from "final-form";
import { connect } from "react-redux";
import ProductForm from "./ProductForm";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";

function UpdateProductForm({ location }) {
  const { state: loadedProduct } = location;
  const { productId } = useParams();
  const [productData, setProductData] = useState(loadedProduct);
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  useEffect(() => {
    if (!productData) {
      Axios.get(`/products/${productId}`)
        .then((product) => {
          setProductData(product.data);
        })
        .catch((err) => history.push("/products/new"));
    }
  }, [productId, productData, history]);
  const updateProduct = React.useCallback(
    (product) => {
      if (Object.keys(product).length < 4) {
        return { [FORM_ERROR]: "Fields are required" };
      }
      setLoading(true);
      let success = false;
      function updateProductSuccess(response) {
        console.log("updated", response);

        success = true;
      }
      function updateProductFail(error) {
        if (error.response) {
          return { [FORM_ERROR]: error.response.data.error };
        }
        return { [FORM_ERROR]: "Unknown error" };
      }
      function redirectIfSuccessful() {
        if (success) {
          history.push("/");
          return;
        }
        setLoading(false);
      }

      return Axios.put(`/products/${productId}`, {
        ...product,
      })
        .then(updateProductSuccess, updateProductFail)
        .finally(redirectIfSuccessful);
    },
    [history, productId]
  );
  return (
    <div className="max-w-sm mx-auto">
      <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
        <h1 className="text-lg text-center mb-4 font-semibold">
          Update Product
        </h1>
        <ProductForm
          initialValues={productData}
          handleFormSubmit={updateProduct}
          submitButtonLabel="Update Product"
          loading={loading}
        />
      </div>
      <p className="text-center text-gray-500 text-xs">
        &copy;2020 hyphenized. All rights reserved.
      </p>
    </div>
  );
}

function mapStateToProps({ account }) {
  return {
    currentCompany: account.user.companyId,
  };
}

export default connect(mapStateToProps)(UpdateProductForm);
