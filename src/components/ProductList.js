import React, { useState, useEffect } from "react";
import Axios from "axios";
import Product from "./Product";
import { Link } from "react-router-dom";
export function ProductList() {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    function getProductsSuccess(response) {
      setProducts(response.data);
    }
    Axios.get("/products").then(getProductsSuccess);
  }, []);
  return (
    <div className="mt-16 bg-white">
      <div className="pt-16 py-4 container max-w-screen-lg mx-auto">
        <header className="flex justify-between mb-8 font-semibold text-sm">
          <h3>All products</h3>
          <Link to="/products/new">Add product</Link>
        </header>
        <div className="grid grid-cols-4 gap-6">
          {products.map((props) => (
            <Product key={props.id} {...props} />
          ))}
        </div>
      </div>
    </div>
  );
}
