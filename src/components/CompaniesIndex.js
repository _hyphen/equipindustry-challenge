import React, { useCallback, useState, useEffect } from "react";
import Axios from "axios";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

function CompaniesIndex({ currentCompany, updateUserProfile }) {
  const [companies, setCompanies] = useState([]);

  useEffect(() => {
    Axios.get("/companies").then((resp) => setCompanies(resp.data));
  }, []);
  const joinCompany = useCallback(
    (companyId) => {
      if (currentCompany) {
        return alert("You must first leave your current company");
      }
      function JoinCompanySuccess(response) {
        updateUserProfile({ companyId: response.data.id });
      }
      function JoinCompanyFail(err) {
        console.log("Could not join company", err);
      }
      Axios.post("/companies/join", { companyId }).then(
        JoinCompanySuccess,
        JoinCompanyFail
      );
    },
    [updateUserProfile, currentCompany]
  );
  const leaveCurrentCompany = useCallback(() => {
    function LeaveCompanySuccess() {
      updateUserProfile({ companyId: null });
    }
    Axios.post("/companies/leave").then(LeaveCompanySuccess);
  }, [updateUserProfile]);
  return (
    <div className="container mx-auto">
      <header className="px-6 sm:px-8 bg-white shadow-sm rounded-sm p-4 text-2xl">
        <span className="text-base">Welcome</span>
        {currentCompany ? (
          <>
            <br />
            <button className="text-sm underline" onClick={leaveCurrentCompany}>
              Leave current company
            </button>
          </>
        ) : (
          <>
            <h2>Choose the company you belong to</h2>
            <Link
              className="inline-block uppercase px-4 py-2 rounded-sm text-xs font-bold text-white bg-blue-500"
              to="/companies/new"
            >
              Create new company
            </Link>
          </>
        )}
      </header>
      <div className="flex mt-8 justify-center">
        {companies.map(({ id, name, ruc }) => (
          <CompanyCard
            key={id}
            id={id}
            onJoinCompany={joinCompany}
            name={name}
            ruc={ruc}
          />
        ))}
      </div>
    </div>
  );
}

function CompanyCard({ id, onJoinCompany, name, ruc }) {
  const handleJoin = useCallback(() => onJoinCompany(id), [id, onJoinCompany]);
  return (
    <div
      key={id}
      onClick={handleJoin}
      className="cursor-pointer mx-4 shadow-md"
    >
      <div className="py-16 px-16 bg-blue-200" />
      <div className="text-center px-6 py-4 bg-blue-600 text-white">
        <h3 className="text-xl">{name}</h3>
        <p className="text-sm">R.U.C: {ruc}</p>
        <span className="text-xs border-t mt-2 py-2 border-white block font-bold uppercase">
          Choose company
        </span>
      </div>
    </div>
  );
}

function mapStateToProps({ account }) {
  return {
    currentCompany: account.user.companyId,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateUserProfile: (user) => dispatch({ type: "UPDATE_PROFILE", user }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CompaniesIndex);
