import React from "react";
import { Switch, Route } from "react-router";
import NewProductForm from "./NewProductForm";
import UpdateProductForm from "./UpdateProductForm";

function Products() {
  return (
    <Switch>
      <Route exact path="/products/new" component={NewProductForm} />
      <Route exact path="/products/:productId" component={UpdateProductForm} />
    </Switch>
  );
}

export default Products;
