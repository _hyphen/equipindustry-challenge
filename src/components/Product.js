import React, { useMemo } from "react";
import { Link } from "react-router-dom";

function Product({ id, companyId, name, sku, price, discount }) {
  const linkData = useMemo(
    (_) => ({
      pathname: `/products/${id}`,
      state: { name, sku, price, discount },
    }),
    [id, name, sku, price, discount]
  );
  return (
    <div className="bg-blue-100 px-2 pt-2 pb-4 rounded-sm" key={id}>
      <header className="text-right mb-24">
        <span className="rounded-full h-8 w-8 text-xs text-white inline-flex items-center justify-center bg-blue-500">
          -{discount}%
        </span>
      </header>
      <div className="text-center">
        <h4 className="text-lg font-semibold">{name}</h4>
        <span className="block text-xs font-semibold mb-4">
          ${price.toFixed(2)}
        </span>
        <Link
          className="inline-block bg-blue-600 px-2 pb-1 rounded-sm text-white"
          to={linkData}
        >
          View Details
        </Link>
      </div>
    </div>
  );
}

export default Product;
