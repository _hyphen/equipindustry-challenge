import React, { useCallback } from "react";
import { Link } from "react-router-dom";
import Axios from "axios";
import { connect } from "react-redux";
import { ProductList } from "./ProductList";

function Dashboard({ updateUserProfile, companyId }) {
  const leaveCurrentCompany = useCallback(() => {
    function LeaveCompanySuccess() {
      updateUserProfile({ companyId: null });
    }
    Axios.post("/companies/leave").then(LeaveCompanySuccess);
  }, [updateUserProfile]);

  if (!companyId) {
    return null;
  }
  return (
    <div>
      <header className="container max-w-screen-lg flex mx-auto">
        <div className="flex-1 px-6 sm:pr-3 pt-2 pb-4 bg-white shadow-sm rounded-sm ">
          <div className="text-right">
            {/* TODO: ADD EDIT VIEW */}
            <Link
              className="inline-block px-2 py-2 rounded-sm text-xs font-bold text-blue-500"
              to="/companies/edit"
            >
              Edit company
            </Link>
          </div>
          <h2 className="mb-3 text-2xl">Company Id: </h2>
          <p className="">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde
            tempore dolores, amet deserunt a, earum ex. Enim maiores facilis ut
            praesentium fugit.
          </p>
        </div>
        <div className="flex w-1/3 ml-8">
          <button
            className="text-sm flex-1 uppercase font-semibold text-blue-600"
            onClick={leaveCurrentCompany}
          >
            Leave company
          </button>
        </div>
      </header>
      <ProductList />
    </div>
  );
}

function mapStateToProps({ account }) {
  return {
    companyId: account.user.companyId,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateUserProfile: (user) => dispatch({ type: "UPDATE_PROFILE", user }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
