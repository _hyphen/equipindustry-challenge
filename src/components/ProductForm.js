import React from "react";
import { Form, Field } from "react-final-form";

function ProductForm({
  handleFormSubmit,
  loading,
  submitButtonLabel,
  initialValues,
}) {
  return (
    <Form initialValues={initialValues} onSubmit={handleFormSubmit}>
      {({ handleSubmit, submitError }) => (
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="name"
            >
              Name
            </label>
            <Field
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="name"
              id="name"
              component="input"
              type="text"
              autoFocus
            />
          </div>
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="sku"
            >
              Sku
            </label>
            <Field
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="sku"
              id="sku"
              component="input"
              type="text"
              autoFocus
            />
          </div>
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="price"
            >
              Price ($)
            </label>
            <Field
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="price"
              id="price"
              component="input"
              type="text"
              autoFocus
            />
          </div>
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="discount"
            >
              Discount
            </label>
            <Field
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="discount"
              id="discount"
              component="input"
              type="text"
              autoFocus
            />
          </div>

          <div className="flex justify-between">
            <button
              disabled={loading}
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
              {submitButtonLabel}
            </button>
            {submitError && (
              <p className="w-2/4 text-red-500 text-xs">Error: {submitError}</p>
            )}
          </div>
        </form>
      )}
    </Form>
  );
}

export default ProductForm;
