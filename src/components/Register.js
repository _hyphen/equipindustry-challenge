import React, { useCallback, useState } from "react";
import { Form, Field } from "react-final-form";
import Axios from "axios";
import { FORM_ERROR } from "final-form";
import { connect } from "react-redux";
import { useHistory } from "react-router";

const FormFieldError = ({ touched, error }) => {
  if (touched && error) {
    return <p className="text-red-500 text-xs">{error.toLowerCase()}</p>;
  }
  return null;
};

function Register({ login }) {
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const registerUser = useCallback(
    (new_account) => {
      setLoading(true);
      let didRegister = false;
      function registerSuccess(response) {
        const token = response.data.sessionId;
        try {
          localStorage.setItem("token", token);
        } catch (error) {
          console.log("Could not store token", error);
        }
        login(response.data.sessionId);
        didRegister = true;
      }
      function registerCleanUp() {
        if (didRegister) {
          history.push("/");
          return;
        }
        setLoading(false);
      }
      function registerFail(err) {
        if (err.response) {
          return { [FORM_ERROR]: err.response.data.error };
        }
        return { [FORM_ERROR]: "Unknown error" };
      }

      return Axios.post("/auth/signup", new_account)
        .then(registerSuccess, registerFail)
        .finally(registerCleanUp);
    },
    [history, login]
  );

  const validateForm = useCallback((input) => {
    const validatePresence = (input, ...values) => {
      return values.reduce((missing, val) => {
        if (!input[val]) {
          missing[val] = `${val} is missing`;
        }
        return missing;
      }, {});
    };
    return validatePresence(
      input,
      "name",
      "email",
      "password",
      "passwordConfirmation"
    );
  }, []);
  return (
    <div className="w-full mx-auto max-w-xs">
      <Form onSubmit={registerUser} validate={validateForm}>
        {({ handleSubmit, submitError, errors, touched }) => (
          <form
            onSubmit={handleSubmit}
            className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
          >
            <div className="mb-4">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="name"
              >
                Name
              </label>
              <Field
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                name="name"
                id="name"
                component="input"
                type="text"
                placeholder="Name"
                autoFocus
              />
              <FormFieldError error={errors.name} touched={touched.name} />
            </div>
            <div className="mb-4">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="reg_email"
              >
                Email
              </label>
              <Field
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                name="email"
                id="reg_email"
                component="input"
                type="email"
                placeholder="Email"
              />
              <FormFieldError error={errors.email} touched={touched.email} />
            </div>
            <div className="mb-4">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="password"
              >
                Password
              </label>
              <Field
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                id="password"
                name="password"
                component="input"
                type="password"
                placeholder="******************"
              />
              <FormFieldError
                error={errors.password}
                touched={touched.password}
              />
            </div>
            <div className="mb-6">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="passwordConfirmation"
              >
                Confirm your password
              </label>
              <Field
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                id="passwordConfirmation"
                name="passwordConfirmation"
                component="input"
                type="password"
                placeholder="******************"
              />
              <FormFieldError
                error={errors.passwordConfirmation}
                touched={touched.passwordConfirmation}
              />
            </div>

            <div className="flex justify-between">
              <button
                disabled={loading}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="submit"
              >
                Sign Up
              </button>
              {submitError && (
                <p className="w-2/4 text-red-500 text-xs">
                  Error: {submitError}
                </p>
              )}
            </div>
          </form>
        )}
      </Form>
      <p className="text-center text-gray-500 text-xs">
        &copy;2020 hyphenized. All rights reserved.
      </p>
    </div>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    login: (token) => dispatch({ type: "LOGIN", token }),
  };
}

export default connect(null, mapDispatchToProps)(Register);
