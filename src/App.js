import React, { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  useRouteMatch,
} from "react-router-dom";
import Home from "./components/Home";
import Dashboard from "./components/Dashboard";
import Header from "./Layout/Header";
import Login from "./components/Login";
import Register from "./components/Register";
import EditPassword from "./components/EditPassword";
import Products from "./components/Products";
import Companies from "./components/Companies";
import { PrivateRoute } from "./PrivateRoutes";
import "./overrides.css";
import { connect } from "react-redux";
import Axios from "axios";

const CrappyLoader = () => (
  <div className="text-2xl h-full flex items-center justify-center">
    <h2>Loading...</h2>
  </div>
);

const RedirectToPickCompany = () => {
  const match = useRouteMatch(["/companies", "/profile"]);

  // Always re-renders
  const AlwaysRedirect = () => <Redirect to="/companies" />;

  if (!match) {
    return <AlwaysRedirect />;
  }
  return null;
};

function App({ isLoggedIn, userData, setUserProfile }) {
  const [ready, setReady] = useState(false);
  const hasCompany = userData && userData.companyId;

  useEffect(() => {
    setReady(false);
    const token = localStorage.getItem("token");

    if (token) {
      Axios.defaults.headers["Authorization"] = token;
    }

    function loadUserProfile(response) {
      setUserProfile(response.data);
    }

    function clearInvalidToken(error) {
      console.log(error, "cleared token");
      localStorage.removeItem("token");
    }
    // app load
    if ((!isLoggedIn && token) || (isLoggedIn && !userData)) {
      Axios.put("/users/me")
        .then(loadUserProfile, clearInvalidToken)
        .finally(() => setReady(true));
      return;
    }

    setReady(true);
  }, [isLoggedIn, userData, setUserProfile]);

  if (!ready) {
    return (
      <div className="h-screen">
        <CrappyLoader />
      </div>
    );
  }
  return (
    <Router>
      <Header />
      {isLoggedIn ? (
        <>
          {!hasCompany && <RedirectToPickCompany />}
          <Route exact path="/" component={Dashboard} />
        </>
      ) : (
        <Route exact path="/" component={Home} />
      )}
      <Route path="/login" component={Login} />
      <Route path="/register" component={Register} />
      <PrivateRoute path="/products" component={Products} />
      <PrivateRoute path="/companies" component={Companies} />
      <Route path="/edit_password" component={EditPassword} />
    </Router>
  );
}

function mapStateToProps({ account }) {
  return {
    isLoggedIn: account.isLoggedIn,
    userData: account.user,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setUserProfile: (user) => dispatch({ type: "LOAD_PROFILE", user }),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
