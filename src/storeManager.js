import { combineReducers } from "redux";

const initialState = {
  isLoggedIn: false,
};

function accountReducer(state = initialState, action) {
  switch (action.type) {
    case "LOAD_PROFILE":
      return {
        ...state,
        isLoggedIn: true,
        user: action.user,
      };
    case "UPDATE_PROFILE":
      return {
        ...state,
        user: {
          ...state.user,
          ...action.user,
        },
      };
    case "LOGIN":
      return {
        ...state,
        isLoggedIn: true,
        token: action.token,
      };
    case "LOGOUT":
      return initialState;
    default:
      return state;
  }
}
const storeManager = combineReducers({ account: accountReducer });
export default storeManager;
