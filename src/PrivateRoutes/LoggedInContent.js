import { connect } from "react-redux";

export const LoggedInContent = ({ isLoggedIn, children }) => {
  if (isLoggedIn) {
    return children;
  }
  return null;
};

function mapStateToProps({ account }) {
  return {
    isLoggedIn: account.isLoggedIn,
  };
}

export default connect(mapStateToProps)(LoggedInContent);
