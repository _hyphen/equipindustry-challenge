import React from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router";

const PrivateRoute = ({ isLoggedIn, component: Component, ...props }) => {
  return (
    <Route {...props}>
      {!isLoggedIn ? <Redirect to="/login" /> : <Component />}
    </Route>
  );
};

function mapStateToProps({ account }) {
  return {
    isLoggedIn: account.isLoggedIn,
  };
}

export default connect(mapStateToProps)(PrivateRoute);
