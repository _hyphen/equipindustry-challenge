import React, { useCallback } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Axios from "axios";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

const VisitorLinks = () => (
  <>
    <li className="ml-auto">
      <Link to="/login">login</Link>
    </li>
    <li className="mx-2">
      <Link to="/register">register</Link>
    </li>
  </>
);

const UserLinks = ({ name, dispatchLogout }) => {
  const history = useHistory();
  const handleLogout = useCallback(
    (e) => {
      e.preventDefault();
      Axios.delete("/auth/logout");
      delete Axios.defaults.headers["Authorization"];
      history.push("/");
      dispatchLogout();
      try {
        localStorage.removeItem("token");
      } catch (error) {
        console.log("Could not remove token from store");
      }
    },
    [history, dispatchLogout]
  );
  return (
    <>
      <li className="ml-auto">
        <Link to="/profile">{name}</Link>
      </li>
      <li className="mx-2">
        <a href="/" onClick={handleLogout}>
          logout
        </a>
      </li>
    </>
  );
};

function Header({ isLoggedIn, username, handleLogout }) {
  return (
    <header className="py-2 px-4 shadow-md mb-8 bg-white">
      <nav className="container max-w-screen-lg mx-auto">
        <ul className="flex uppercase text-sm font-medium">
          <li className="mx-2">
            <Link to="/">home</Link>
          </li>
          <li className="mx-2">
            <Link to="/companies">companies</Link>
          </li>
          <li className="mx-2">
            <Link to="/products">products</Link>
          </li>
          {isLoggedIn ? (
            <UserLinks dispatchLogout={handleLogout} name={username} />
          ) : (
            <VisitorLinks />
          )}
        </ul>
      </nav>
    </header>
  );
}

function mapStateToProps({ account }) {
  return {
    isLoggedIn: account.isLoggedIn,
    username: account.user?.name,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    handleLogout: () => dispatch({ type: "LOGOUT" }),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
